CFLAGS = -Wall -g
CC = gcc
SYSTEM_H = StackList

.PHONY: compile clean

compile: math.o $(SYSTEM_H).o
	$(CC) -g $^ -o math

math.o: math.c Stack.h $(SYSTEM_H).o
	$(CC) $(CFLAGS) -D SYSTEM_H=$(SYSTEM_H).h -c math.c
$(SYSTEM_H).o: $(SYSTEM_H).c $(SYSTEM_H).h
	$(CC) $(CFLAGS) -c $(SYSTEM_H).c
clean:
	rm -f *.o math

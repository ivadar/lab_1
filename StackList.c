/*
* Реализация стека на основе списка для первой лабы
*/

#include "StackList.h"
#include <stdlib.h>
#include <stdio.h>
#define SIZE 100
#define SUCCESS 0
#define ERROR_STACK -100
#define STACK_OVERFLOW -101
#define STACK_UNDERFLOW -102
#define EMPTY 1
#define NOT_EMPTY 0

int init(PStack stack)
{
    if(!stack)
        return ERROR_STACK;
    stack->top = NULL;
    return SUCCESS;
}

int push(PStack stack, union Data toPush)
{
    if (!stack)
        return ERROR_STACK;
    PNode pnode = (PNode)malloc(sizeof(Node));
    if (!pnode)
        return STACK_OVERFLOW;
    pnode->data = toPush;
    pnode->next = stack->top;
    stack->top = pnode;
    return SUCCESS;
}

int pop(const PStack stack, PData destination)
{
    if (!stack)
        return ERROR_STACK;
    if (!destination)
        return ERROR_STACK;
    if (!stack->top)
        return ERROR_STACK;
    *destination = stack->top->data;
    PNode node = stack->top;
    stack->top = stack->top->next;
    free(node);
    return SUCCESS;
}

int top(const PStack stack, PData destination)
{
    if (!stack)
        return ERROR_STACK;
    if (!destination)
        return ERROR_STACK;
    if (!stack->top)
        return ERROR_STACK;
    *destination = stack->top->data;
    return SUCCESS;
}

int isEmpty(const PStack stack)
{
    if (!stack)
        return ERROR_STACK;
    if (stack->top)
        return NOT_EMPTY;
    return EMPTY;
}

int clear(PStack stack)
{
    if (!stack)
        return ERROR_STACK;
    Data destination;
    while (stack->top)
    {
        if (pop(stack, &destination))
            return ERROR_STACK;
    }
    return SUCCESS;
}


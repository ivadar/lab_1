#pragma once

#include "DataTypes.h"
#include <stdio.h>

struct StackBasedOnArray
{
	enum DataType dataType;
	PData data;
	size_t topIndex; // or pointer to the top element/next to the top element
	size_t stackSize;
};

typedef struct StackBasedOnArray Stack, *PStack;

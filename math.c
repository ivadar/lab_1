#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "Stack.h"

char * infixToPostfix(char *str, PStack ps)
{
    size_t len = strlen(str), z = 0, bracket = 0;
    char *postfix = calloc(len, sizeof(str));
    const char* copy = str;
    char var[1];
    Data data;
    PData pdata = &data;
    int curstate, prevstate = NOTHING;
    do
    {
        switch(*copy)
        {
            case ' ': case '\n': case '\0':
                continue;

            case '(':
                if (prevstate == NUMBER)
                {
                    printf("There is no operator between parenthesis and number\n");
                    return "!";
                }
                curstate = NOTHING;
                data.operate = 40;     // 40 == "("
                push(ps, data);
                bracket++;
                break;

            case ')':
                curstate = NOTHING;
                top(ps, pdata);
                strcat(postfix, ",");
                while (pdata->operate != '(')
                {
                    pop(ps, pdata);
                    *var = pdata->operate;
                    strncat(postfix, var, 1);
                    strcat(postfix, ",");
                    if (isEmpty(ps))
                        break;
                    top(ps, pdata);
                }
                if (pdata->operate == '(')
                    pop(ps, pdata);
                bracket--;
                break;

            case '+': case '-':
                curstate = OPERATOR;
                if ((prevstate == NOTHING || prevstate == OPERATOR) && (*copy != '+'))
                {
                    strncat(postfix, copy, 1);
                    z++;
                    break;
                }
                else
                {
                    strcat(postfix, ",");  
                    z=0;      
                }
                top(ps, pdata);
                while (pdata->operate == '+' || pdata->operate == '-' || pdata->operate == '*' || pdata->operate == '/')
                {
                    pop(ps, pdata);
                    *var = pdata->operate;
                    strncat(postfix, var, 1);
                    strcat(postfix, ",");
                    if (isEmpty(ps))
                        break;
                    top(ps, pdata);
                }
                data.operate = *copy;
                push(ps, data);
                break;

            case '*': case '/':
                curstate = OPERATOR;
                top(ps, pdata);
                if (prevstate != NOTHING)
                    strcat(postfix, ",");
                while (pdata->operate == '*' || pdata->operate == '/')
                {
                    pop(ps, pdata);
                    *var = pdata->operate;
                    strncat(postfix, var, 1);
                    strcat(postfix, ",");
                    if (isEmpty(ps))
                        break;
                    top(ps, pdata);
                }
                data.operate = *copy;
                push(ps, data);
                break;    

            case '0': case '1': case '2': 
            case '3': case '4': case '5':
            case '6': case '7':	case '8': 
            case '9':
                curstate = NUMBER;
                strncat(postfix, copy, 1);
                break;
            default:
                printf("Malformed expression!\n");
                return "!";
        }
        if(z > 1)
        {
            printf("Malformed expression!\n");
            return "!";
        }
        prevstate = curstate;
    } while (*copy++);
    while (isEmpty(ps) != 1)
    {
        strcat(postfix, ",");
        pop(ps, pdata);
        *var = pdata->operate;
        strncat(postfix, var, 1);
    }
    if (!isEmpty(ps))
        clear(ps);
    if (bracket != 0)
    {
        printf("Malformed expression!\n");
        return("!");
    }
    return(postfix);
}

int calculations(char *str, PStack ps)
{
    Data data;
    PData pdata;
    pdata = &data;
    char *res = strtok(str, ",");
    do
    {
        int temp_one, temp_two;
       
        while (*res != '+' && *res != '-' && *res != '*' && *res != '/')
        {       
            data.number = atoi(res);
            push(ps, data);
            res = strtok (NULL, ",");
            if (res)
                continue;
            else 
            {
                printf("ans = %d\n", pdata -> number);
                if (!isEmpty(ps))
                    clear(ps);
                return 0;
            }
        }
        if (*res == '-' && isdigit(*(res + 1)))
        {
            data.number = atoi(res);
            push(ps, data);
            res = strtok (NULL, ",");
            if (res)
                continue;
            else 
            {
                printf("ans = %d\n", pdata -> number);
                if (!isEmpty(ps))
                    clear(ps);
                return 0;
            }
        }        
        pop(ps, pdata);
        temp_two = pdata->number;
        if (isEmpty(ps))
        {
            printf("Malformed expression!\n");
            return 0;
        }
        pop(ps, pdata); 
        temp_one = pdata->number;
        switch (*res)
        {
            case '+':
                data.number = temp_one + temp_two;
                push(ps, data);
                break;
            case '-':
                data.number = temp_one - temp_two;
                push(ps, data);
                break;
            case '*':
                data.number = temp_one * temp_two;
                push(ps, data);
                break;
            case '/':
                if (temp_two == 0)
                {
                    printf("You cannot divide by zero!\n");
                    return -1;
                }
                data.number = temp_one / temp_two;
                push(ps, data);
                break;                
        }
        res = strtok (NULL, ",");
    } while (res);
    top(ps, pdata);
    printf("ans = %d\n", pdata -> number);
    if (!isEmpty(ps))
        clear(ps);
    return 0;
}

int main()
{
    int flag = 1;
    char enter[100];
    memset(enter, 0, 100);
    printf("Ivanchenko Darya Vladimirovna\n");
    printf("Group M8O-110B-80\n");
    while (flag)
    {
        printf(">> ");
        fgets(enter, 100, stdin);
        enter[strlen(enter)-1] = '\0';
        if (strcmp(enter, "author") == 0)
        {
            printf("Ivanchenko Darya Vladimirovna\n");
            memset(enter, 0, 100);
            continue;
        }
        else if (strcmp(enter, "quit") == 0)
        {
            printf("Goodbue!\n");
            flag = 0;
        }
        else 
        {
            Stack stack;
            PStack ps = &stack;
            init(ps);
            char *postfix = infixToPostfix(enter, ps);
            if (*postfix == '!')
            {
                memset(enter, 0, 100);
                continue;
            }
            int result = calculations(postfix, ps);
            if (result == -1)
            {
                memset(enter, 0, 100);
                continue;
            }
        }
    }
    return 0;
}
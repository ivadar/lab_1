#pragma once

enum DataType { NOTHING, NUMBER, OPERATOR };

union Data
{
	int number;
	char operate;
};

typedef union Data Data, * PData;
/*
* Реализация стека на основе массива для первой лабы
*/

#include "StackArray.h"
#include <stdlib.h>
#include <stdio.h>
#define SIZE 100
#define SUCCESS 0
#define ERROR_STACK -100
#define STACK_OVERFLOW -101
#define STACK_UNDERFLOW -102
#define EMPTY 1
#define NOT_EMPTY 0

int init(PStack stack)
{
    stack->data = (Data*)malloc(sizeof(Data)*SIZE);
    if (!stack->data)
        return ERROR_STACK;
    stack->topIndex = 0;
    stack->stackSize = SIZE;
    return SUCCESS;
}

int push(PStack stack, union Data toPush)
{
    if (!stack)
        return ERROR_STACK;
    if (stack->topIndex >= stack->stackSize)
        stack->data = (PData)realloc(stack->data, stack->stackSize * 2);
    if (!stack->data)
        return STACK_OVERFLOW;
    stack->topIndex++;
	stack->data[stack->topIndex] = toPush;
    return SUCCESS;
}

int pop(const PStack stack, PData destination)
{
    if (!stack)
    return ERROR_STACK;
    if (stack->topIndex == 0)
        return STACK_UNDERFLOW;
    *destination = stack->data[stack->topIndex];
    stack->topIndex--;
    return SUCCESS;
}

int top(const PStack stack, PData destination)
{
    if (!stack)
		return ERROR_STACK;
    if (stack->topIndex == 0)
      return STACK_UNDERFLOW;
    *destination = stack->data[stack->topIndex];
    return SUCCESS;
}

int isEmpty(const PStack stack)
{
    if (!stack)
      return ERROR_STACK;
    if (stack->topIndex == 0)
      return EMPTY;
    return NOT_EMPTY;
}

int clear(PStack stack)
{
    if (!stack)
		return ERROR_STACK;
    stack->topIndex = 0;
    free(stack->data);
    return SUCCESS;
}
